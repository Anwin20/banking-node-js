// banking.test.js

const { deposit, withdraw } = require('../src/banking');
const { transfer } = require('../src/banking');

describe('Banking Functions', () => {
    let user;

    beforeEach(() => {
        user = { id: 1, balance: 100 };
    });

    describe('Deposit Function', () => {
        test('should increase the balance when a positive amount is deposited', () => {
            const newBalance = deposit(user, 50);
            expect(newBalance).toEqual(150);
        });

        test('should throw an error for non-positive amounts', () => {
            expect(() => deposit(user, -50)).toThrow('Invalid amount for deposit');
        });
    });

    describe('Withdraw Function', () => {
        test('should decrease the balance when a valid amount is withdrawn', () => {
            const newBalance = withdraw(user, 50);
            expect(newBalance).toEqual(50);
        });

        test('should throw an error for non-positive amounts', () => {
            expect(() => withdraw(user, -50)).toThrow('Invalid amount for withdrawal');
        });

        test('should throw an error for amounts greater than the balance', () => {
            expect(() => withdraw(user, 150)).toThrow('Invalid amount for withdrawal');
        });
    });

    // describe('Transfer Function', () => {
    //     let recipient;

    //     beforeEach(() => {
    //         recipient = { id: 2, balance: 50 };
    //     });

    //     test('should transfer money between users', () => {
    //         const senderBalanceBefore = user.balance;
    //         const recipientBalanceBefore = recipient.balance;

    //         const amount = 30;

    //         const result = transfer(user, recipient, amount);
    //         const senderBalanceAfter = senderBalanceBefore - amount;
    //         const recipientBalanceAfter = recipientBalanceBefore + amount;
    //         expect(result.senderBalance).toEqual(senderBalanceAfter);
    //         expect(result.recipientBalance).toEqual(recipientBalanceAfter);
    //     });

    //     test('should throw an error for invalid transfer amounts', () => {
    //         expect(() => transfer(user, recipient, -30)).toThrow('Invalid transfer amount or insufficient balance');
    //     });

    //     test('should throw an error for insufficient balance', () => {
    //         expect(() => transfer(user, recipient, 200)).toThrow('Invalid transfer amount or insufficient balance');
    //     });
    // });
});
